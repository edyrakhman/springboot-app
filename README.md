# Functions:
- Login user
- Create, update, delete user
- Check user role

# Libraries:
- JDK 11
- Springboot
- MyBatis
- Postgre
- JWT
- Lombok
- Jasypt
- JMapper

# How to test:
- Clone project using this link: https://gitlab.com/edyrakhman/springboot-app.git
- Build project using maven: mvn clean install
- Run project using java console: java -jar app-1.0.0-RELEASE.jar

