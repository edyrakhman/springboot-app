package id.edyrakhman.app.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JGlobalMap
public class UserLoginRequestDTO {
	
	private String username;
	private String password;

}
