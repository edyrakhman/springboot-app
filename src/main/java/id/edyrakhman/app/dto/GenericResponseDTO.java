package id.edyrakhman.app.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GenericResponseDTO {
	
	private String code;
	private String desc;
	private Object data;
	
	public GenericResponseDTO(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	public GenericResponseDTO(String code, String desc, Object data) {
		this.code = code;
		this.desc = desc;
		this.data = data;
	}

}
