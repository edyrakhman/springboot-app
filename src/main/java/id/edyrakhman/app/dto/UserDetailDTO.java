package id.edyrakhman.app.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDetailDTO {
	
	private String userRole;
	private List<String> menus;

}
