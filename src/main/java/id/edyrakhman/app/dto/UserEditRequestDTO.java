package id.edyrakhman.app.dto;

import java.util.UUID;

import com.googlecode.jmapper.annotations.JGlobalMap;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JGlobalMap
public class UserEditRequestDTO {
	
	private UUID id;
	private String username;
	private String password;
	private String phoneNo;
	private UUID userRoleId;

}
