package id.edyrakhman.app.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import id.edyrakhman.app.dto.UserDetailDTO;
import id.edyrakhman.app.entity.JwtPayload;
import id.edyrakhman.app.entity.User;
import id.edyrakhman.app.enums.ResponseCode;
import id.edyrakhman.app.enums.UserRole;
import id.edyrakhman.app.exception.BusinessException;
import id.edyrakhman.app.mapper.UserMapper;
import id.edyrakhman.app.util.PasswordUtil;

@Service("userService")
public class UserService {
	
	@Autowired
	private UserMapper userMapper;	
	
	public void add(User user) throws Exception {
		userMapper.insert(user);
	}
	
	public void edit(User user, JwtPayload payload) throws Exception {
		String userRole = payload.getUserRole();
		if (userRole.equals(UserRole.USER.name())) {
			user.setId(payload.getId());
			userMapper.updateByUser(user);
		} else if (userRole.equals(UserRole.ADMIN.name())) {
			if (user.getId() == null) {
				throw new BusinessException(ResponseCode.MANDATORY.getCode(), user.getId()+" "+ResponseCode.MANDATORY.getDesc());
			}
			user.setPassword(PasswordUtil.encrypt(user.getPassword()));
			userMapper.updateByAdmin(user);
		}
	}
	
	public void delete(UUID id) throws Exception {
		userMapper.delete(id);
	}
	
	public User selectByUsername(String username) throws Exception {
		return userMapper.selectByUsername(username);
	}
	
	public UserDetailDTO selectById(UUID id) throws Exception {
		return userMapper.selectById(id);
	}
	

}
