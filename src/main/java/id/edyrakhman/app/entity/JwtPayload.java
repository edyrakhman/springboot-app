package id.edyrakhman.app.entity;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JwtPayload {
	
	private UUID id;
	private String username;
	private String userRole;
	

}
