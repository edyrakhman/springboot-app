package id.edyrakhman.app.entity;

import java.util.UUID;

import id.edyrakhman.app.dto.UserAddRequestDTO;
import id.edyrakhman.app.dto.UserEditRequestDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class User extends BaseEntity {
	
	private String username;
	private String password;
	private String phoneNo;
	private UUID userRoleId;
	
	public User(UserAddRequestDTO dto) {
		this.username = dto.getUsername();
		this.password = dto.getPassword();
		this.phoneNo = dto.getPhoneNo();
		this.userRoleId = dto.getUserRoleId();
	}
	
	public User(UserEditRequestDTO dto) {
		this.id = dto.getId();
		this.username = dto.getUsername();
		this.password = dto.getPassword();
		this.phoneNo = dto.getPhoneNo();
		this.userRoleId = dto.getUserRoleId();
	}
	
	
	

}
