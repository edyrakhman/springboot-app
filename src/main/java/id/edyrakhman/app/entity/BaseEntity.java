package id.edyrakhman.app.entity;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseEntity {
	
	protected UUID id;

}
