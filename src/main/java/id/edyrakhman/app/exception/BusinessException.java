package id.edyrakhman.app.exception;

import id.edyrakhman.app.enums.ResponseCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BusinessException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private String responseCode;
	private String responseDesc;

	public BusinessException(String responseCode, String responseDesc) {
		this.responseCode = responseCode;
		this.responseDesc = responseDesc;
	}

}
