package id.edyrakhman.app.mapper;

import java.util.UUID;

import org.apache.ibatis.annotations.Mapper;

import id.edyrakhman.app.dto.UserDetailDTO;
import id.edyrakhman.app.entity.User;

@Mapper
public interface UserMapper {
	
	public void insert(User user) throws Exception;
	
	public void updateByUser(User user) throws Exception;
	
	public void updateByAdmin(User user) throws Exception;
	
	public void delete(UUID id) throws Exception;
	
	public User selectByUsername(String username) throws Exception;
	
	public UserDetailDTO selectById(UUID id) throws Exception;

}
