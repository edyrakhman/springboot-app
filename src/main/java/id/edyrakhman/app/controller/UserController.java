package id.edyrakhman.app.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.edyrakhman.app.dto.GenericResponseDTO;
import id.edyrakhman.app.dto.UserAddRequestDTO;
import id.edyrakhman.app.dto.UserDetailDTO;
import id.edyrakhman.app.dto.UserEditRequestDTO;
import id.edyrakhman.app.dto.UserLoginRequestDTO;
import id.edyrakhman.app.dto.UserLoginResponseDTO;
import id.edyrakhman.app.entity.JwtPayload;
import id.edyrakhman.app.entity.User;
import id.edyrakhman.app.enums.ResponseCode;
import id.edyrakhman.app.enums.UserRole;
import id.edyrakhman.app.exception.BusinessException;
import id.edyrakhman.app.service.UserService;
import id.edyrakhman.app.util.JwtUtil;
import id.edyrakhman.app.util.PasswordUtil;

@RestController
@RequestMapping("/users")
public class UserController { 
	
	@Autowired
	private UserService userService;
	
	@PostMapping
	ResponseEntity<GenericResponseDTO> add(@RequestHeader("Authorization") String auth, @RequestBody UserAddRequestDTO userAddRequestDTO) {
		try {
			checkUserRole(auth);
			
			//User user = JMapperUtil.convert(User.class, UserAddRequestDTO.class, userAddRequestDTO);
			
			User user = new User(userAddRequestDTO);
			user.setPassword(PasswordUtil.encrypt(user.getPassword()));
			
			userService.add(user);
		} catch (BusinessException e) {
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(e.getResponseCode(), e.getResponseDesc()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(ResponseCode.RUNTIME_EXCEPTION.getCode(), ResponseCode.RUNTIME_EXCEPTION.getDesc()));
		}
		return ResponseEntity.ok(new GenericResponseDTO(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc()));
	}
	
	@PutMapping
	ResponseEntity<GenericResponseDTO> edit(@RequestHeader("Authorization") String auth, @RequestBody UserEditRequestDTO userEditRequestDTO) {
		try {
			//User user = JMapperUtil.convert(User.class, UserEditRequestDTO.class, userEditRequestDTO);
			
			User user = new User(userEditRequestDTO);
			
			JwtPayload payload = JwtUtil.extractPayload(auth.substring(7));
			
			userService.edit(user, payload);
		} catch (BusinessException e) {
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(e.getResponseCode(), e.getResponseDesc()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(ResponseCode.RUNTIME_EXCEPTION.getCode(), ResponseCode.RUNTIME_EXCEPTION.getDesc()));
		}
		return ResponseEntity.ok(new GenericResponseDTO(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc()));
	}
	
	@DeleteMapping("/{id}")
	ResponseEntity<GenericResponseDTO> delete(@RequestHeader("Authorization") String auth, @PathVariable UUID id) {
		try {
			checkUserRole(auth);
			
			userService.delete(id);
		} catch (BusinessException e) {
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(e.getResponseCode(), e.getResponseDesc()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(ResponseCode.RUNTIME_EXCEPTION.getCode(), ResponseCode.RUNTIME_EXCEPTION.getDesc()));
		}
		return ResponseEntity.ok(new GenericResponseDTO(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc()));
	}
	
	@PostMapping("/login")
	ResponseEntity<GenericResponseDTO> login(@RequestBody UserLoginRequestDTO userLoginRequestDTO) {
		UserLoginResponseDTO userLoginResponseDTO = new UserLoginResponseDTO();
		try { 
			User user = userService.selectByUsername(userLoginRequestDTO.getUsername());
			if (user != null) {
				if (PasswordUtil.check(userLoginRequestDTO.getPassword(), user.getPassword())) {
					UserDetailDTO userDetailDTO = userService.selectById(user.getId());
					
					JwtPayload payload = new JwtPayload();
					payload.setId(user.getId());
					payload.setUsername(user.getUsername());
					payload.setUserRole(userDetailDTO.getUserRole());
					
					String accessToken = JwtUtil.generateAccessToken(payload);
					userLoginResponseDTO.setAccessToken(accessToken);
					
					String refreshToken = JwtUtil.generateRefreshToken(accessToken);
					userLoginResponseDTO.setRefreshToken(refreshToken);
					
					userLoginResponseDTO.setUserRole(userDetailDTO.getUserRole());
					userLoginResponseDTO.setMenus(userDetailDTO.getMenus());
				} else {
					throw new BusinessException(ResponseCode.USER_OR_PASS_INCORRECT.getCode(), ResponseCode.USER_OR_PASS_INCORRECT.getDesc());
				}
			} else {
				throw new BusinessException(ResponseCode.USER_NOT_FOUND.getCode(), ResponseCode.USER_NOT_FOUND.getDesc());
			}
		} catch (BusinessException e) {
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(e.getResponseCode(), e.getResponseDesc()));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.internalServerError().body(new GenericResponseDTO(ResponseCode.RUNTIME_EXCEPTION.getCode(), ResponseCode.RUNTIME_EXCEPTION.getDesc()));
		}
		return ResponseEntity.ok(new GenericResponseDTO(ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getDesc(), userLoginResponseDTO));
	}
	
	public void checkUserRole(String auth) throws Exception {
		JwtPayload payload = JwtUtil.extractPayload(auth.substring(7));
		if (payload.getUserRole().equals(UserRole.USER.name())) {
			throw new BusinessException(ResponseCode.USER_RESTRICTION.getCode(),ResponseCode.USER_RESTRICTION.getDesc());
		}
	}


}
