package id.edyrakhman.app.mybatis;

import java.util.UUID;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;

import id.edyrakhman.app.entity.BaseEntity;


@Intercepts(@Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }))
public class BaseEntityInterceptor implements Interceptor {
	
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
		SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
		Object parameter = invocation.getArgs()[1];
		if (parameter instanceof BaseEntity) {
			BaseEntity baseEntity = (BaseEntity) parameter;
			if (SqlCommandType.INSERT.equals(sqlCommandType)) {
				baseEntity.setId(UUID.randomUUID());
			} 
		}
		return invocation.proceed();
	}

}
