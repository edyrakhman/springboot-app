package id.edyrakhman.app.util;

import org.jasypt.util.password.BasicPasswordEncryptor;

public class PasswordUtil {

	public static boolean check(String inputPassword, String encryptPassword) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		try {
			if (passwordEncryptor.checkPassword(inputPassword, encryptPassword)) {
				return true;
			}
		}catch (Exception e){
			return inputPassword.equals(encryptPassword);
		}
		return false;
	}

	public static String encrypt(String password) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		return passwordEncryptor.encryptPassword(password);
	}
	
	public static void main(String[] args) {
		System.out.println(encrypt("password1234"));
	}
	

}
