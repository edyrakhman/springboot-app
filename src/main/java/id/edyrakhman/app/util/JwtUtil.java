package id.edyrakhman.app.util;

import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

import com.fasterxml.jackson.core.type.TypeReference;

import id.edyrakhman.app.entity.JwtPayload;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class JwtUtil {

	public static long accessTokenExpiry;
	public static String accessTokenSecret;

	public static long refreshTokenExpiry;
	public static String refreshTokenSecret;

	public static String subjectAccessToken;
	public static String subjectRefreshToken;

	static {
		accessTokenExpiry = 1 * 60 * 60; // 1 hour
		accessTokenSecret = "0OaKJQP6faHtI7okMPD1qrER8qOL4Ks5ChOOXtfMLO4=";
		subjectAccessToken = "jwt.accesstoken.app";
		
		refreshTokenExpiry = 6 * 60 * 60; // 6 hour
		refreshTokenSecret = "nE0cMkFxBGaixnrBB06NOJsoIGsuHisDKejSHnYmlRg=";
		subjectRefreshToken = "jwt.refreshtoken.app";

	}

	public static Claims extractAllClaims(String token, String secret) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	public static String generateAccessToken(JwtPayload jwtPayload) {
		HashMap<String, Object> payload = JacksonUtil.mapper.convertValue(jwtPayload, new TypeReference<HashMap<String, Object>>() {});
		return Jwts.builder()
				.setClaims(payload)
				.setSubject(subjectAccessToken)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * accessTokenExpiry))
				.signWith(SignatureAlgorithm.HS256, accessTokenSecret).compact();
	}

	public static String renewAccessToken(String refreshToken) {
		Claims claims = extractAllClaims(refreshToken, refreshTokenSecret);
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subjectAccessToken)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * accessTokenExpiry))
				.signWith(SignatureAlgorithm.HS256, accessTokenSecret).compact();
	}

	public static String generateRefreshToken(String accessToken) {
		Claims claims = extractAllClaims(accessToken, accessTokenSecret);
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subjectRefreshToken)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * refreshTokenExpiry))
				.signWith(SignatureAlgorithm.HS256, refreshTokenSecret).compact();
	}

	public static void validateAccessToken(String token) throws Exception {
		extractAllClaims(token, accessTokenSecret);
	}
	
	public static void validateRefreshToken(String token) throws Exception {
		extractAllClaims(token, refreshTokenSecret);
	}

	
	public static JwtPayload extractPayload(String token) throws Exception {
		String[] chunks = token.split("\\.");
		
		Base64.Decoder decoder = Base64.getDecoder();

		String payload = new String(decoder.decode(chunks[1]));
		
		JwtPayload jwtPayload = JacksonUtil.mapper.readValue(payload, JwtPayload.class);
		
		return jwtPayload;
	}
}
