package id.edyrakhman.app.util;

import com.googlecode.jmapper.JMapper;

public class JMapperUtil {
	
	public static <D,S> D convert(Class<D> destClass, Class<S> sourceClass, S sourceObj){
		JMapper<D,S> jMapper = new JMapper<>(destClass, sourceClass);
        return jMapper.getDestination(sourceObj);
    }

}
