package id.edyrakhman.app.enums;

public enum ResponseCode {
	
	SUCCESS("0","Success"),	
	RUNTIME_EXCEPTION("1","Runtime exception"),	
	USER_OR_PASS_INCORRECT("2","User or password incorrect"),
	USER_NOT_FOUND("3","User not found"),
	USER_RESTRICTION("4","User dont have authority to do this operation"),
	MANDATORY("5","is required");

    private String code;
    private String desc;

    ResponseCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }
    
    public String getDesc() {
        return desc;
    }

}
