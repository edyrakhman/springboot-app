package id.edyrakhman.app.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import id.edyrakhman.app.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String requestTokenHeader = request.getHeader("Authorization");
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			String token = requestTokenHeader.substring(7);
			try {
				JwtUtil.validateAccessToken(token);
				filterChain.doFilter(request, response);
			} catch (ExpiredJwtException e) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.getOutputStream().print("{ \"code\": \"1\", \"desc\": \"Token expired\" }");
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				return;
			} catch (Exception e) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.getOutputStream().print("{ \"code\": \"1\", \"desc\": \"Token invalid\" }");
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				return;
			}	
		} else {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getOutputStream().print("{ \"code\": \"1\", \"desc\": \"Token is required\" }");
			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
			return;
		}
		
	}
	
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request)
	  throws ServletException {
	    String path = request.getRequestURI();
	    return path.startsWith("/users/login");
	}

}
